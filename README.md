# T3H - RE2205 ( React JS )

> Reference: [Link](https://t3h.edu.vn/dao-tao-ngan-han/lap-trinh-frontend-reactjs)

### Lessons

- HTML & CSS ( 5 parts )
- JavaScript ( 10 parts )
- React JS ( 13 parts )


### Contact

- Lecturers: [TruongDQ](https://www.facebook.com/laclac.dev/)
- Github: [TruongDQ](https://github.com/truongnat)